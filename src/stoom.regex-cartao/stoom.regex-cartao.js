/**
 * EXPRESSÕES REGULARES (REGEX) DOS BIN'S DOS CARTÕES DE CRÉDITOS
 * Script com as regex dos cartões de crédito sinalizados abaixo na tabela.
 * 
 * Para utilizar, basta dar include nesse JS, e usar o objeto RegexCartao, chamando o método de validação de cada
 * cartão. Por exemplo, para validar o cartão visa, usar o método RegexCartao.visaText([string com o número do cartão]).
 * A função retorna TRUE se o número do cartão for compatível com a regex daquele cartão, e FALSE caso contrário.
 * 
 ***********************************************************************************************************************
  _________________________________________________________________________________________________________________
 | Tem Regex  | Bandeira   | Comeca com                                  | Máximo de número | Máximo de número cvc |
 | ---------- | ---------- | ------------------------------------------- | ---------------- | -------------------- |
 |    Sim     | Visa       | 4                                           | 13, 16           | 3                    |
 |    Sim     | Mastercard | 2, 51 à 55                                  | 16               | 3                    |
 |    Sim     | Diners     | 301 à 305, 36, 38                           | 14, 16           | 3                    |
 |    Sim     | Elo        | Vários                                      | 16               | 3                    |
 |    Sim     | Amex       | 34, 37                                      | 15               | 4                    |
 |    Sim     | Aura       | 50                                          | 16               | 3                    |
 |    Sim     | Hipercard  | 38, 60                                      | 13, 16, 19       | 3                    |
 |    Sim     | Hiper      | 6370                                        | 16               | 3                    |
 |    Sim     | Credz      | 636760, 637032                              | 16               | 3                    |
 |    Não     | jcb        | 35                                          | 16               | 3                    |
 |    Não     | Discover   | 6011, 622, 64, 65                           | 16               | 4                    |
 |____________|____________|_____________________________________________|__________________|______________________|

**/

var regexVisa = "(4[0-9]{3}(([\\s]*)[0-9]{4}){2}([\\s]*)[0-9]{1}(?:[0-9]{3})?)";
var regexMastercard = "((2[0-9]{3}|5[1-5][0-9]{2})(([\\s]*)[0-9]{4}){3})";
var regexDinners = "(3(0[0-5]|[68][0-9])[0-9]{1}(([\\s]*)[0-9]{4}){2}(?:([\\s]*)[0-9]{2})?)";
var regexHipercard = "((38|60)[0-9]{2}(([\\s]*)[0-9]{4}){2}(([\\s]*)[0-9])(?:[0-9]{3}([\\s]*)[0-9]{3}|[0-9]{3})?)";
var regexHiper = "(6370(([\\s]*)[0-9]{4}){3})";
var regexAmericanExpress = "((34|37)([0-9]{2})(([\\s]*)[0-9]{4}){2}(([\\s]*)[0-9]{3}))";
var regexElo = "((6363([\\s]*)6[8-9]|4389([\\s]*)35|5041([\\s]*)75|4514([\\s]*)16|4011([\\s]*)79|4312([\\s]*)74|4573([\\s]*)93|6362([\\s]*)97|6277([\\s]*)80|4576([\\s]*)3[1-2]|4011([\\s]*)78|5066([\\s]*)99|5067([\\s]*)[0-6][0-9]|5067([\\s]*)7[0-8]|509[0-9]([\\s]*)[0-9]{2}|6500([\\s]*)3[1-3]|6500([\\s]*)3[5-9]|6500([\\s]*)4[0-9]|6500([\\s]*)5[0-1]|6504([\\s]*)0[5-9]|6504([\\s]*)[1-3][0-9]|6504([\\s]*)8[5-9]|6504([\\s]*)9[0-9]|6505([\\s]*)[0-2][0-9]|6505([\\s]*)3[0-8]|6505([\\s]*)[4-8][1-9]|6505([\\s]*)9[0-8]|6507([\\s]*)0[0-9]|6507([\\s]*)1[0-8]|6507([\\s]*)2[0-7]|6509([\\s]*)0[1-9]|6509([\\s]*)1[0-9]|6509([\\s]*)20|6516([\\s]*)5[2-9]|6516([\\s]*)[6-7][0-9]|6550([\\s]*)[0-1][0-9]|6550([\\s]*)2[1-9]|6550([\\s]*)[2-4][0-9]|6550([\\s]*)5[0-8])[0-9]{2}(([\\s]*)[0-9]{4}){2}?)";
var regexAura = "((50[0-9]{2})(([\\s]*)[0-9]{4}){3})";
var regexCredz = "((63(67|70))([\\s]*)((60|32)[0-9]{2})(([\\s]*)[0-9]{4}){2})";

var RegexCartao = {

	visa : new RegExp("^" + regexVisa + "$"),

	mastercard : new RegExp("^" + regexMastercard + "$"),

	dinners : new RegExp("^" + regexDinners + "$"),

	hipercard : new RegExp("^" + regexHipercard + "$"),

	hiper : new RegExp("^" + regexHiper + "$"),

	americanExpress : new RegExp("^" + regexAmericanExpress + "$"),

	elo : new RegExp("^" + regexElo + "$"),

	aura : new RegExp("^" + regexAura + "$"),

	credz : new RegExp("^" + regexCredz + "$"),

	visaTest : function(cardNumber) {
		return this.visa.test(tiraEspacos(cardNumber));
	},

	mastercardTest : function(cardNumber) {
		return this.mastercard.test(tiraEspacos(cardNumber));
	},

	dinnersTest : function(cardNumber) {
		return this.dinners.test(tiraEspacos(cardNumber));
	},

	hipercardTest : function(cardNumber) {
		return this.hipercard.test(tiraEspacos(cardNumber));
	},

	hiperTest : function(cardNumber) {
		return this.hiper.test(tiraEspacos(cardNumber));
	},

	americanExpressTest : function(cardNumber) {
		return this.americanExpress.test(tiraEspacos(cardNumber));
	},

	eloTest : function(cardNumber) {
		return this.elo.test(tiraEspacos(cardNumber));
	},

	auraTest : function(cardNumber) {
		return this.aura.test(tiraEspacos(cardNumber));
	},

	credzTest : function(cardNumber) {
		return this.credz.test(tiraEspacos(cardNumber));
	},

	getConcatAllRegex : function() {
		return "^" + regexVisa + "|" + regexMastercard + "|" + regexDinners + "|" + regexHipercard + "|" + 
				regexHiper + "|" + regexAmericanExpress + "|" + regexElo + "|" + regexAura + "|" + regexCredz + "$";
	}
}

/**
 * Tira os espaços do texto passado por parâmetro.
 * 
 * @param text - Texto do qual será retirado os espaços.
 * @returns texto sem os espaços.
 */
function tiraEspacos(text) {
	return text.replace(/ /g,'');
}
