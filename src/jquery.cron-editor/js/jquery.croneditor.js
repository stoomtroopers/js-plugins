jQuery.fn.croneditor = function(opts) {

  var el = this;
  
  // Write the HTML template to the document
  jQuery(el).html(tmpl);

  var cronArr = ["*", "*", "*", "*", "*", "?"];
  if (typeof opts.value === "string") {
    cronArr = opts.value.split(' ');
  }

  jQuery( ".tabs" ).tabs({
    activate: function( event, ui ) {
      switch (jQuery(ui.newTab).attr('id')) {

        // Seconds
        case 'button-second-every':
          cronArr[0] = "*";
        break;
        case 'button-second-n':
          cronArr[0] = "0/" + jQuery( "#tabs-second .slider" ).slider("value");
        break;
        case 'button-second-each':
          cronArr[0] = "*";
          jQuery('.tabs-second-format').html('');
          drawEachSeconds();
        break;

        // Minutes
        case 'button-minute-every':
          cronArr[1] = "*";
        break;
        case 'button-minute-n':
          cronArr[1] = "0/" + jQuery( "#tabs-minute .slider" ).slider("value");
        break;
        case 'button-minute-each':
          cronArr[1] = "*";
          // TODO: toggle off selected minutes on load
          //jQuery('.tabs-minute-format input[checked="checked"]').click()
          jQuery('.tabs-minute-format').html('');
          drawEachMinutes();
        break;

        // Hours
        case 'button-hour-every':
           cronArr[2] = "*";
        break;
        case 'button-hour-n':
          cronArr[2] = "0/" + jQuery( "#tabs-hour .slider" ).slider("value");
        break;
        case 'button-hour-each':
          cronArr[2] = "*";
          jQuery('.tabs-hour-format').html('');
          drawEachHours();
         break;

         // Days
         case 'button-day-every':
            cronArr[3] = "*";
         break;
         case 'button-day-each':
           cronArr[3] = "*";
           jQuery('.tabs-day-format').html('');
           drawEachDays();
          break;

          // Months
          case 'button-month-every':
             cronArr[4] = "*";
          break;
          case 'button-month-each':
            cronArr[4] = "*";
            jQuery('.tabs-month-format').html('');
            drawEachMonths();
           break;

           // Weeks
           case 'button-week-every':
              cronArr[5] = "*";
           break;
           case 'button-week-each':
             cronArr[5] = "*";
             jQuery('.tabs-week-format').html('');
             drawEachWeek();
            break;

      }

      drawCron();
    }
  });

  function drawCron () {

    var newCron = cronArr.join(' ');
    jQuery('#cronString').val(newCron);
    // TODO: add back next estimated cron time
    /*
    var last = new Date();
    jQuery('.next').html('');
    var job = new cron.CronTime(newCron);
    var next = job._getNextDateFrom(new Date());
    jQuery('.next').append('<span id="nextRun">' + dateformat(next, "ddd mmm dd yyyy HH:mm:ss") + '</span><br/>');
    */
    /*
    setInterval(function(){
      drawCron();
    }, 500);
    */
    /*
    jQuery('#cronString').keyup(function(){
      cronArr = jQuery('#cronString').val().split(' ');
      console.log('updated', cronArr)
    });
    */
  }

  jQuery('#clear').click(function(){
    jQuery('#cronString').val('* * * * * ?');
    cronArr = ["*","*","*","*","*", "?"];
    
  });

  jQuery( "#tabs-second .slider" ).slider({
    min: 1,
    max: 59,
    slide: function( event, ui ) {
      cronArr[0] = "0/" + ui.value;
      jQuery('#tabs-second-n .preview').html('Every ' + ui.value + ' seconds');
      drawCron();
    }
  });

  jQuery( "#tabs-minute .slider" ).slider({
    min: 1,
    max: 59,
    slide: function( event, ui ) {
      cronArr[1] = "0/" + ui.value;
      jQuery('#tabs-minute-n .preview').html('Every ' + ui.value + ' minutes');
      drawCron();
    }
  });

  jQuery( "#tabs-hour .slider" ).slider({
    min: 1,
    max: 23,
    slide: function( event, ui ) {
      cronArr[2] = "0/" + ui.value;
      jQuery('#tabs-hour-n .preview').html('Every ' + ui.value + ' Hours');
      drawCron();
    }
  });

  // TOOD: All draw* functions can be combined into a few smaller methods

  function drawEachSeconds () {
    // minutes
    for (var i = 0; i < 60; i++) {
      var padded = i;
      if(padded.toString().length === 1) {
        padded = "0" + padded;
      }
      jQuery('.tabs-second-format').append('<input type="checkbox" id="second-check' + i + '"><label for="second-check' + i + '">' + padded + '</label>');
      if (i !== 0 && (i+1) % 10 === 0) {
        jQuery('.tabs-second-format').append('<br/>');
      }
    }
    jQuery('.tabs-second-format input').button();
    jQuery('.tabs-second-format').buttonset();

    jQuery('.tabs-second-format input[type="checkbox"]').click(function(){
      var newItem = jQuery(this).attr('id').replace('second-check', '');
      if(cronArr[0] === "*") {
        cronArr[0] = jQuery(this).attr('id').replace('second-check', '');
      } else {

        // if value already in list, toggle it off
        var list = cronArr[0].split(',');
        if (list.indexOf(newItem) !== -1) {
          list.splice(list.indexOf(newItem), 1);
          cronArr[0] = list.join(',');
        } else {
          // else toggle it on
          cronArr[0] = cronArr[0] + "," + newItem;
        }
        if(cronArr[0] === "") {
          cronArr[0] = "*";
        }
      }
      drawCron();
    });
  }

  function drawEachMinutes () {
    // minutes
    for (var i = 0; i < 60; i++) {
      var padded = i;
      if(padded.toString().length === 1) {
        padded = "0" + padded;
      }
      jQuery('.tabs-minute-format').append('<input type="checkbox" id="minute-check' + i + '"><label for="minute-check' + i + '">' + padded + '</label>');
      if (i !== 0 && (i+1) % 10 === 0) {
        jQuery('.tabs-minute-format').append('<br/>');
      }
    }
    jQuery('.tabs-minute-format input').button();
    jQuery('.tabs-minute-format').buttonset();

    jQuery('.tabs-minute-format input[type="checkbox"]').click(function(){
      var newItem = jQuery(this).attr('id').replace('minute-check', '');
      if(cronArr[1] === "*") {
        cronArr[1] = jQuery(this).attr('id').replace('minute-check', '');
      } else {

        // if value already in list, toggle it off
        var list = cronArr[1].split(',');
        if (list.indexOf(newItem) !== -1) {
          list.splice(list.indexOf(newItem), 1);
          cronArr[1] = list.join(',');
        } else {
          // else toggle it on
          cronArr[1] = cronArr[1] + "," + newItem;
        }
        if(cronArr[1] === "") {
          cronArr[1] = "*";
        }
      }
      drawCron();
    });
  }
  
  function drawEachHours () {
    // hours
    for (var i = 0; i < 24; i++) {
      var padded = i;
      if(padded.toString().length === 1) {
        padded = "0" + padded;
      }
      jQuery('.tabs-hour-format').append('<input type="checkbox" id="hour-check' + i + '"><label for="hour-check' + i + '">' + padded + '</label>');
      if (i !== 0 && (i+1) % 12 === 0) {
        jQuery('.tabs-hour-format').append('<br/>');
      }
    }

    jQuery('.tabs-hour-format input').button();
    jQuery('.tabs-hour-format').buttonset();


    jQuery('.tabs-hour-format input[type="checkbox"]').click(function(){
      var newItem = jQuery(this).attr('id').replace('hour-check', '');
      if(cronArr[2] === "*") {
        cronArr[2] = jQuery(this).attr('id').replace('hour-check', '');
      } else {

        // if value already in list, toggle it off
        var list = cronArr[2].split(',');
        if (list.indexOf(newItem) !== -1) {
          list.splice(list.indexOf(newItem), 1);
          cronArr[2] = list.join(',');
        } else {
          // else toggle it on
          cronArr[2] = cronArr[2] + "," + newItem;
        }
        if(cronArr[2] === "") {
          cronArr[2] = "*";
        }
      }
      drawCron();
    });

  };

  function drawEachDays () {

    // days
    for (var i = 1; i < 32; i++) {
      var padded = i;
      if(padded.toString().length === 1) {
        padded = "0" + padded;
      }
      jQuery('.tabs-day-format').append('<input type="checkbox" id="day-check' + i + '"><label for="day-check' + i + '">' + padded + '</label>');
      if (i !== 0 && (i) % 7 === 0) {
        jQuery('.tabs-day-format').append('<br/>');
      }
    }

    jQuery('.tabs-day-format input').button();
    jQuery('.tabs-day-format').buttonset();

    jQuery('.tabs-day-format input[type="checkbox"]').click(function(){
      var newItem = jQuery(this).attr('id').replace('day-check', '');
      if(cronArr[3] === "*") {
        cronArr[3] = jQuery(this).attr('id').replace('day-check', '');
      } else {

        // if value already in list, toggle it off
        var list = cronArr[3].split(',');
        if (list.indexOf(newItem) !== -1) {
          list.splice(list.indexOf(newItem), 1);
          cronArr[3] = list.join(',');
        } else {
          // else toggle it on
          cronArr[3] = cronArr[3] + "," + newItem;
        }
        if(cronArr[3] === "") {
          cronArr[3] = "*";
        }

      }
      drawCron();
    });

  };


  function drawEachMonths () {
    // months
    var months = [null, 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];

    for (var i = 1; i < 13; i++) {
      var padded = i;
      if(padded.toString().length === 1) {
        //padded = "0" + padded;
      }
      jQuery('.tabs-month-format').append('<input type="checkbox" id="month-check' + i + '"><label for="month-check' + i + '">' + months[i] + '</label>');
    }

    jQuery('.tabs-month-format input').button();
    jQuery('.tabs-month-format').buttonset();


    jQuery('.tabs-month-format input[type="checkbox"]').click(function(){
      var newItem = jQuery(this).attr('id').replace('month-check', '');
      if(cronArr[4] === "*") {
        cronArr[4] = jQuery(this).attr('id').replace('month-check', '');
      } else {

        // if value already in list, toggle it off
        var list = cronArr[4].split(',');
        if (list.indexOf(newItem) !== -1) {
          list.splice(list.indexOf(newItem), 1);
          cronArr[4] = list.join(',');
        } else {
          // else toggle it on
          cronArr[4] = cronArr[4] + "," + newItem;
        }
        if(cronArr[4] === "") {
          cronArr[4] = "*";
        }

      }
      drawCron();
    });

  };

  function drawEachWeek () {
    // weeks
    var days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
    for (var i = 0; i < 7; i++) {
      var padded = i;
      if(padded.toString().length === 1) {
        //padded = "0" + padded;
      }

      jQuery('.tabs-week-format').append('<input type="checkbox" id="week-check' + i + '"><label for="week-check' + i + '">' + days[i] + '</label>');
    }

    jQuery('.tabs-week-format input').button();
    jQuery('.tabs-week-format').buttonset();

    jQuery('.tabs-week-format input[type="checkbox"]').click(function(){
      var newItem = jQuery(this).attr('id').replace('week-check', '');
      if(cronArr[5] === "*") {
        cronArr[5] = jQuery(this).attr('id').replace('week-check', '');
      } else {

        // if value already in list, toggle it off
        var list = cronArr[5].split(',');
        if (list.indexOf(newItem) !== -1) {
          list.splice(list.indexOf(newItem), 1);
          cronArr[5] = list.join(',');
        } else {
          // else toggle it on
          cronArr[5] = cronArr[5] + "," + newItem;
        }
        if(cronArr[5] === "") {
          cronArr[5] = "*";
        }

      }
      drawCron();
    });

  };

  // TODO: Refactor these methods into smaller methods
  drawEachSeconds();
  drawEachMinutes();
  drawEachHours();
  drawEachDays();
  drawEachMonths();
  drawCron();
};

// HTML Template for plugin
var tmpl = '<input type="text" id="cronString" name="thread.cronExpression" value="* * * * * ?" size="80"/>\
<button type="button" onclick="testarCronExpression();">Testar cron</button>\
<input type="button" value="Reset" id="clear"/>\
<br/>\
<div id="testCron" style="display: none;"><span>Lista das datas/horários gerados pelo cron:</span><br/></div>\
<!-- TODO: add back next estimated time -->\
<!-- <span>Will run next at:<em><span class="next"></span></em></span> -->\
<!-- the cron editor will be here -->\
<div id="tabs" class="tabs">\
  <ul>\
    <li><a href="#tabs-second">Segundo</a></li>\
    <li><a href="#tabs-minute">Minuto</a></li>\
    <li><a href="#tabs-hour">Hora</a></li>\
    <li><a href="#tabs-day">Dia do Mês</a></li>\
    <li><a href="#tabs-month">Mês</a></li>\
    <li><a href="#tabs-week">Dia da Semana</a></li>\
  </ul>\
  <div id="tabs-second">\
    <div class="tabs">\
      <ul>\
        <li id="button-second-every"><a href="#tabs-second-every">A cada segundo</a></li>\
        <li id="button-second-n"><a href="#tabs-second-n">A cada n segundos</a></li>\
		<li id="button-second-each"><a href="#tabs-second-each">Cada Segundo Selecionado</a></li>\
      </ul>\
      <div id="tabs-second-every" class="preview">\
        <div>*</div>\
        <div>Todo segundo.</div>\
      </div>\
      <div id="tabs-second-n">\
        <div class="preview">A cada 1 segundo</div>\
        <div class="slider"></div>\
      </div>\
      <div id="tabs-second-each" class="preview">\
        <div>Cada Segundo Selecionado</div><br/>\
        <div class="tabs-second-format"></div>\
      </div>\
    </div>\
  </div>\
  <div id="tabs-minute">\
    <div class="tabs">\
      <ul>\
        <li id="button-minute-every"><a href="#tabs-minute-every">A cada minuto</a></li>\
        <li id="button-minute-n"><a href="#tabs-minute-n">A cada n minutos</a></li>\
        <li id="button-minute-each"><a href="#tabs-minute-each">Cada Minuto Selecionado</a></li>\
      </ul>\
      <div id="tabs-minute-every" class="preview">\
        <div>*</div>\
        <div>A cada minuto.</div>\
      </div>\
      <div id="tabs-minute-n">\
        <div class="preview">A cada 1 minuto</div>\
        <div class="slider"></div>\
      </div>\
      <div id="tabs-minute-each" class="preview">\
        <div>Cada Minuto Selecionado</div><br/>\
        <div class="tabs-minute-format"></div>\
      </div>\
    </div>\
  </div>\
  <div id="tabs-hour">\
    <div class="tabs">\
      <ul>\
        <li id="button-hour-every"><a href="#tabs-hour-every">A cada hora</a></li>\
        <li id="button-hour-n"><a href="#tabs-hour-n">A cada n horas</a></li>\
        <li id="button-hour-each"><a href="#tabs-hour-each">Cada Hora Selecionada</a></li>\
      </ul>\
      <div id="tabs-hour-every" class="preview">\
        <div>*</div>\
        <div>A cada hora</div>\
      </div>\
      <div id="tabs-hour-n">\
        <div class="preview">A cada 1 hora</div>\
        <div class="slider"></div>\
      </div>\
      <div id="tabs-hour-each" class="preview">\
        <div>Cada Hora Selecionada</div><br/>\
        <div class="tabs-hour-format"></div>\
      </div>\
    </div>\
  </div>\
  <div id="tabs-day">\
    <div class="tabs">\
      <ul>\
        <li id="button-day-every"><a href="#tabs-day-every">A cada dia</a></li>\
        <li id="button-day-each"><a href="#tabs-day-each">Cada Dia</a></li>\
      </ul>\
      <div id="tabs-day-every" class="preview">\
        <div>*</div>\
        <div>A cada dia</div>\
      </div>\
      <div id="tabs-day-each" class="preview">\
        <div>Cada Dia Selecionado</div><br/>\
        <div class="tabs-day-format"></div>\
      </div>\
    </div>\
  </div>\
  <div id="tabs-month">\
    <div class="tabs">\
      <ul>\
        <li id="button-month-every"><a href="#tabs-month-every">A cada mês</a></li>\
        <li id="button-month-each"><a href="#tabs-month-each">Cada mês</a></li>\
      </ul>\
      <div id="tabs-month-every" class="preview">\
        <div>*</div>\
        <div>A cada mês</div>\
      </div>\
      <div id="tabs-month-each" class="preview">\
        <div>Cada Mês Selecionado</div><br/>\
        <div class="tabs-month-format"></div>\
      </div>\
    </div>\
  </div>\
  <div id="tabs-week">\
    <div class="tabs">\
      <ul>\
        <li id="button-week-every"><a href="#tabs-week-every">A cada semana</a></li>\
        <li id="button-week-each"><a href="#tabs-week-each">Cada semana</a></li>\
      </ul>\
      <div id="tabs-week-every" class="preview">\
        <div>*</div>\
        <div>A cada dia</div>\
      </div>\
      <div id="tabs-week-each">\
        <div class="preview">Cada Dia Selecionado</div><br/>\
        <div class="tabs-week-format"></div>\
      </div>\
    </div>\
  </div>\
</div>';