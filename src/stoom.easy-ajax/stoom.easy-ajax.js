/**
 * Explicar melhor como se usa esse plugin, alguns exemplos ou um link pra wiki mais claro
 * Para mostrar o throbber e overlay precisa do stoom.easy-throbber
 * 
 * por wesley.ide e luigi.labigalini
 * 
 */

var sAjax = {
	ajax : function(initParams) {
		this.debug = initParams.debug ? true : false;
		var url = initParams.url;
		var params = initParams.params;
		var toSelector = initParams.toSelector;
		var showThrobber = initParams.throbber;
		var showOverlay = initParams.overlay;
		var complete = initParams.complete;
		var success = initParams.success;
		var error = initParams.error;

		if (url === undefined) {
			sAjax.log("Erro : URL vazia...");
			return false;
		}
		jQuery("body").addClass("s-throbber-wait");

		var $_throbber = null;
		var $_overlay = null;
		if (showThrobber != false && typeof $sThrobber !== 'undefined'){
			var throbberDestination = toSelector;
			sAjax.log("Throbber...");
			if (showOverlay == true) {
				sAjax.log("Mostrando overlay...");
				$_overlay = jQuery("<div class='s-overlay'></div>");
				$_overlay.appendTo(toSelector);
				throbberDestination = $_overlay; 
			} else {
				sAjax.log("Escondendo filhos...");
				jQuery(toSelector).children().hide();				
			}
			sAjax.log("Clonando throbber...");
			$_throbber = $sThrobber.clone();
			$_throbber.appendTo(throbberDestination).fadeIn(800);
		}

		url = ctx + "/" + url;
		sAjax.log("Url : " + url);

		var fields = new Array();
		if (params !== undefined) {
			if (typeof(params) === 'string' || params instanceof String) {
				sAjax.log("Params é string, serializando array de inputs para usar como fields do ajax...");
				fields = jQuery(params + " :input").serializeArray();
				jQuery.each(fields, function(i, field){
					 field.value = field.value;
				});
			} else if (params instanceof Array) {
				sAjax.log("Params é array, utilizando como fields do ajax...");
				fields = params;
			}
		}
		sAjax.log("Fields : " + fields);

		sAjax.log("Fazendo chamada ajax...");
		jQuery.ajax({
			url: url,
			data: fields,
			method: 'POST',
			success: function(data){
				sAjax.log("Sucesso : " + data);
				if ((typeof(data) === 'string'|| data instanceof String) && toSelector !== undefined) {
					sAjax.log("Enviando dados para toSelector : " + toSelector);
					if(toSelector instanceof jQuery)
						toSelector.html(data);
					else {
						toSelector.split(",").forEach(function (item) {
							if (jQuery(data).closest(item).length) {
								jQuery(item).replaceWith(jQuery(data).closest(item));
							} else if (jQuery(data).find(item).length) {
								jQuery(item).replaceWith(jQuery(data).find(item));
							} else {
								if($_throbber != null)
									$_throbber.remove();
								if($_overlay != null)
									$_overlay.remove();
								else
									jQuery(item).children().show();
								jQuery(item).append(data);
							}
						});
					}
				}
				if(success !== undefined){
					sAjax.log("Chamando função success parametrizada.");
					success(data);
				}
			},
			error: function(data){
				if(error !== undefined){
					sAjax.log("Chamando função error parametrizada.");
					error(data);
				}
			},
			complete: function(data){
				if(complete !== undefined){
					sAjax.log("Chamando função complete parametrizada.");
					complete(data);
				}
			}
		}).done(function(){
			jQuery("body").removeClass("s-throbber-wait");
		});

		return this;
	},
	log : function (info) {
		if(sAjax.debug)
			console.log("sAjax -> " + info);
	}
}
