/*
 * This file is a Stoom easy use for the Tabulator package. It depends on:
 * -> stoom.jquery.easy-ajax
 * -> tabulator
 * 
 * To use it just call $element.sTabulator(options)
 * - Options:
 * -- url : where to get the json to build the table
 * -- timeout : seconds to refresh the table
 * 
 * To automate the build step just use the class "s-tabulator" and the options as a json on a data-stab='({url:..., timeout:...})'.
 *
 * (c) Luigi Labigalini <luigi@stoom.com.br>
 *
 * Full Documentation & Demos of the original Tabulator can be found at: http://olifolkerd.github.io/tabulator/
 *
 */

(function(){
	
	jQuery.widget("ui.sTabulator", {

	options: {
		url:null,
		timeout:null, // TODO usar o timeout setInterval(myFunction, 3000), talvez criar uma funcao refresh
    },

	////////////////// Element Construction //////////////////

	_create: function(){
		var self = this;
		var options = self.options;
		var element = self.element;

		element.addClass('s-tabulator');
		
		self._load(self);
		if(options.timeout != 'undefined' && options.timeout != null)
			setInterval(function() { self._load(self); }, options.timeout);
	},

	_load: function(self){
		var options = self.options;
		var element = self.element;

		sAjax.ajax({
			toSelector: element,
			url: options.url,
			success: function(data){
				if(data.tables[0].tablejson != null) {
					var table = eval(data.tables[0].tablejson);
					if(element.hasClass('tabulator'))
						element.tabulator('destroy');
					element.tabulator(table.options);
					element.tabulator("setData", table.data);
				}
			}
		});
	},
	
	refresh: function(){
		var self = this;
		self._load(self);
	},
		
	////////////////// Element Desconstructor //////////////////

	//deconstructor
	_destroy: function(){
		var self = this;
		var element = self.element;
		element.tabulator('destroy');
		element.removeClass("s-tabulator");
	},

});

})();

//Procura por divs com classe s-tabulator e já carrega a tabela
$(function(){
	jQuery("[data-stab]").each(function () {
		jQuery(this).sTabulator(eval(jQuery(this).data('stab')));
	});
});