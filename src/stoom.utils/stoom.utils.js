/**
 * Explicar melhor pra que existe esse plugin e o que pode vir aqui
 * 
 * por wesley.ide e luigi.labigalini
 * 
 */

////Popula jQuery.browser.mobile
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

function isString(s) {
    return typeof(s) === 'string' || s instanceof String;
}

// deprecated, procurar onde usa e trocar por pattern html5
function validarEmail(email) {
	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	return filter.test(email);
}

function getCookie(name) {
	var cookiesGlobal = document.cookie;
	var start = cookiesGlobal.indexOf(name+"=");
	var len = start+name.length+1;
	if ((!start) && (name != cookiesGlobal.substring(0,name.length))) return null;
	if (start == -1) return null;
	var end = cookiesGlobal.indexOf(";",len);
	if (end == -1) end = cookiesGlobal.length;

	return cookiesGlobal.substring(len,end);
}

function isMobile(){
	return jQuery.browser.mobile;
}

function trim(str){
	str = str.replace(/^\s+|\s+$/g,"");
	return str;
}

function validaCNPJ(strCnpj) {
	var cnpj = strCnpj.replace(/[.-]+/g,"").replace("/","");
	if (cnpj.length != 14 ||
			cnpj == "00000000000000" ||
			cnpj == "11111111111111" ||
			cnpj == "22222222222222" ||
			cnpj == "33333333333333" ||
			cnpj == "44444444444444" ||
			cnpj == "55555555555555" ||
			cnpj == "66666666666666" ||
			cnpj == "77777777777777" ||
			cnpj == "88888888888888" ||
			cnpj == "99999999999999")
		 return false;

	var soma = 0, dig;
	var cnpj_calc = cnpj.substring(0,12);
	var chr_cnpj = cnpj;
	for(var i = 0; i < 4; i++){
		if(chr_cnpj.charAt(i) >=0 && chr_cnpj.charAt(i) <= 9)
			soma += (chr_cnpj.charAt(i)) * (6 - (i+1)) ;
	}
	for( var i = 0; i < 8; i++ ){
		if (chr_cnpj.charAt(i+4) >=0 && chr_cnpj.charAt(i+4) <= 9)
			soma += (chr_cnpj.charAt(i+4)) * (10 - (i+1));
	}
	dig = 11 - (soma%11);
	cnpj_calc += ( dig == 10 || dig == 11 ) ? "0" : dig;
	soma = 0;
	for(var i=0; i<5; i++)
		if(chr_cnpj.charAt(i)>=0 && chr_cnpj.charAt(i) <= 9)
			soma += (chr_cnpj.charAt(i))*(7 - (i+1));
	for (var i=0; i<8; i++)
		if(chr_cnpj.charAt(i+5) >=0 && chr_cnpj.charAt(i+5) <= 9)
			soma += (chr_cnpj.charAt(i+5)) * (10 - (i+1));
	dig = 11 - (soma%11);
	cnpj_calc += ( dig == 10 || dig == 11 ) ? "0" : dig;
	return (cnpj == cnpj_calc);
}

function validaCPF(strCPF){
	var strCpf = strCPF.replace(/[\s.-]+/g,"");
	if(strCpf.length != 11 ||
				strCpf == "00000000000" ||
				strCpf == "11111111111" ||
				strCpf == "22222222222" ||
				strCpf == "33333333333" ||
				strCpf == "44444444444" ||
				strCpf == "55555555555" ||
				strCpf == "66666666666" ||
				strCpf == "77777777777" ||
				strCpf == "88888888888" ||
				strCpf == "99999999999")
			return false;

	var d1, d2;
	var digito1, digito2, resto;
	var digitoCPF;
	var nDigResult;

	d1 = d2 = 0;
	digito1 = digito2 = resto = 0;

	for (var nCount = 1; nCount < strCpf.length -1; nCount++)	{
		digitoCPF = strCpf.substring(nCount -1, nCount);
		d1 = d1 + ( 11 - nCount ) * digitoCPF;
		d2 = d2 + ( 12 - nCount ) * digitoCPF;
	}
	resto = (d1 % 11);

	if (resto < 2) digito1 = 0;
	else digito1 = 11 - resto;

	d2 += 2 * digito1;

	resto = (d2 % 11);

	if (resto < 2) digito2 = 0;
	else digito2 = 11 - resto;

	var nDigVerific = strCpf.substring (strCpf.length -2, strCpf.length);

	nDigResult = digito1+""+digito2;

	if(nDigVerific != nDigResult) return false;
	else return true;
}

function validaCEP(strCEP){
	var cep = strCEP.replace(/-/g,"");

	if((typeof cep==='number' && (cep%1)===0) && cep.length == 8)
		return true

	return false
}