/**
 * Para usar o um throbber no seu projeto deve ser colocadas duas variaveis disponiveis para todo o js:
 * 
 * <link href="plugins/stoom.easy-throbber/css/stoom.easy-throbber.NOMEDOTHROBER.css" rel="stylesheet" type="text/css" />
 * 
 * <script type="text/javascript" src="plugins/stoom.easy-throbber/stoom.easy-throbber.js"></script>
 * <script>
 *   var _sThrobber = _sThrobberNOMEDOTHROBBER; // String com elemento HTML
 *   var $sThrobber = jQuery(_sThrobber); // jQuery do elemento
 * </script>
 * 
 * por luigi.labigalini
 * 
 */

var _sThrobberSkCircle =
	'<div class="s-throbber sk-circle">'+
		'<div class="sk-circle1 sk-child"></div>'+
		'<div class="sk-circle2 sk-child"></div>'+
		'<div class="sk-circle3 sk-child"></div>'+
		'<div class="sk-circle4 sk-child"></div>'+
		'<div class="sk-circle5 sk-child"></div>'+
		'<div class="sk-circle6 sk-child"></div>'+
		'<div class="sk-circle7 sk-child"></div>'+
		'<div class="sk-circle8 sk-child"></div>'+
		'<div class="sk-circle9 sk-child"></div>'+
		'<div class="sk-circle10 sk-child"></div>'+
		'<div class="sk-circle11 sk-child"></div>'+
		'<div class="sk-circle12 sk-child"></div>'+
	'</div>';

var _sThrobberSkCubeGrid = 
	'<div class="s-throbber sk-cube-grid">' +
		'<div class="sk-cube sk-cube1"></div>' +
		'<div class="sk-cube sk-cube2"></div>' +
		'<div class="sk-cube sk-cube3"></div>' +
		'<div class="sk-cube sk-cube4"></div>' +
		'<div class="sk-cube sk-cube5"></div>' +
		'<div class="sk-cube sk-cube6"></div>' +
		'<div class="sk-cube sk-cube7"></div>' +
		'<div class="sk-cube sk-cube8"></div>' +
		'<div class="sk-cube sk-cube9"></div>' +
	'</div>';

var _sThrobberSpinner = 
	'<div class="s-throbber spinner">' +
		'<div class="bounce1"></div>' +
		'<div class="bounce2"></div>' +
		'<div class="bounce3"></div>' +
	'</div>';