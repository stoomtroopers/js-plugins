/*!
 * Infinite Ajax Scroll, a jQuery plugin
 * Version v0.1.5
 * http://webcreate.nl/
 *
 * Copyright (c) 2011-2012 Jeroen Fiege
 * Licensed under the MIT License:
 * http://webcreate.nl/license
 * 
 * Alterado por luigi.labigalini
 * Agora funciona com jQuery 3+
 * 
 * Alterado por victor.pandolfe
 * Retirando unbind -> o() quando é última página
 * 
 */
! function(t) {
    t.ias = function(e) {
        function n() {
            return m.onChangePage(function(t, e, n) {
                w && w.setPage(t, n), d.onPageChange.call(this, t, n, e)
            }), i(), w && w.havePage() && (o(), pageNum = w.getPage(), p.forceScrollTop(function() {
                pageNum > 1 ? (l(pageNum), curTreshold = s(!0), t("html,body").scrollTop(curTreshold)) : i()
            })), P
        }

        function i() {
            r(), t(window).scroll(a)
        }

        function a() {
            scrTop = t(window).scrollTop(), wndHeight = t(window).height(), curScrOffset = scrTop + wndHeight, curScrOffset >= s() && u(curScrOffset)
        }

        function o() {
            t(window).unbind("scroll", a)
        }

        function r() {
            t(d.pagination).hide()
        }

        function s(e) {
            return el = t(d.container).find(d.item).last(), 0 == el.length ? 0 : (treshold = el.offset().top + el.height(), e || (treshold += d.tresholdMargin), treshold)
        }

        function u(e, n) {
            return urlNextPage = t(d.next).attr("href"), urlNextPage ? (m.pushPages(e, urlNextPage), o(), f(), void c(urlNextPage, function(e, a) {
                result = d.onLoadItems.call(this, a), result !== !1 && (t(a).hide(), curLastItem = t(d.container).find(d.item).last(), curLastItem.after(a), t(a).fadeIn()), t(d.pagination).replaceWith(t(d.pagination, e)), g(), i(), d.onRenderComplete.call(this, a), n && n.call(this)
            })) : null
        }

        function c(e, n) {
            var i = [];
            t.get(e, null, function(e) {
                t(d.container, e).find(d.item).each(function() {
                    i.push(this)
                }), n && n.call(this, e, i)
            }, "html")
        }

        function l(e) {
            curTreshold = s(!0), curTreshold > 0 && u(curTreshold, function() {
                o(), m.getCurPageNum(curTreshold) + 1 < e ? (l(e), t("html,body").animate({
                    scrollTop: curTreshold
                }, 400, "swing")) : (t("html,body").animate({
                    scrollTop: curTreshold
                }, 1e3, "swing"), i())
            })
        }

        function h() {
            return loader = t(".ias_loader"), 0 == loader.length && (loader = t("<div class='ias_loader'>" + d.loader + "</div>"), loader.hide()), loader
        }

        function f(e) {
            loader = h(), el = t(d.container).find(d.item).last(), el.after(loader), loader.fadeIn()
        }

        function g() {
            loader = h(), loader.remove()
        }
        var d = t.extend({}, t.ias.defaults, e),
            p = new t.ias.util,
            m = new t.ias.paging,
            w = d.history ? new t.ias.history : !1,
            P = this;
        n()
    }, t.ias.defaults = {
        container: "#container",
        item: ".item",
        pagination: "#pagination",
        next: ".next",
        loader: '<img src="images/loader.gif"/>',
        tresholdMargin: 0,
        history: !0,
        onPageChange: function() {},
        onLoadItems: function() {},
        onRenderComplete: function() {}
    }, t.ias.util = function() {
        function e() {
            t(window).on('load', function() {
                n = !0
            })
        }
        var n = !1,
            i = !1,
            a = this;
        e(), this.forceScrollTop = function(e) {
            t("html,body").scrollTop(0), i || (n ? (e.call(), i = !0) : setTimeout(function() {
                a.forceScrollTop(e)
            }, 1))
        }
    }, t.ias.paging = function() {
        function e() {
            t(window).scroll(n)
        }

        function n() {
            scrTop = t(window).scrollTop(), wndHeight = t(window).height(), curScrOffset = scrTop + wndHeight, curPageNum = a(curScrOffset), curPagebreak = o(curScrOffset), u != curPageNum && s.call(this, curPageNum, curPagebreak[0], curPagebreak[1]), u = curPageNum
        }

        function a(t) {
            for (i = r.length - 1; i > 0; i--)
                if (t > r[i][0]) return i + 1;
            return 1
        }

        function o(t) {
            for (i = r.length - 1; i >= 0; i--)
                if (t > r[i][0]) return r[i];
            return null
        }
        var r = [
                [0, document.location.toString()]
            ],
            s = function() {},
            u = 1;
        e(), this.getCurPageNum = function(t) {
            return a(t)
        }, this.onChangePage = function(t) {
            s = t
        }, this.pushPages = function(t, e) {
            r.push([t, e])
        }
    }, t.ias.history = function() {
        function t() {
            n = !!(window.history && history.pushState && history.replaceState), n = !1
        }
        var e = !1,
            n = !1;
        t(), this.setPage = function(t, e) {
            this.updateState({
                page: t
            }, "", e)
        }, this.havePage = function() {
            return 0 != this.getState()
        }, this.getPage = function() {
            return this.havePage() ? (stateObj = this.getState(), stateObj.page) : 1
        }, this.getState = function() {
            if (n) {
                if (stateObj = history.state, stateObj && stateObj.ias) return stateObj.ias
            } else if (haveState = "#/page/" == window.location.hash.substring(0, 7), haveState) return pageNum = parseInt(window.location.hash.replace("#/page/", "")), {
                page: pageNum
            };
            return !1
        }, this.updateState = function(t, n, i) {
            e ? this.replaceState(t, n, i) : this.pushState(t, n, i)
        }, this.pushState = function(t, i, a) {
            n ? history.pushState({
                ias: t
            }, i, a) : (hash = t.page > 0 ? "#/page/" + t.page : "", window.location.hash = hash), e = !0
        }, this.replaceState = function(t, e, i) {
            n ? history.replaceState({
                ias: t
            }, e, i) : this.pushState(t, e, i)
        }
    }
}(jQuery);